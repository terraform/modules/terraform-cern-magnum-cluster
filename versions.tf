# Define required OpenStack providers

terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.51.0"
    }
  }
}
