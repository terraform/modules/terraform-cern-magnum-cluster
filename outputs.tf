# Defining output values to be printed out

# Print Cluster id value
output "cluster_id" {
  description = "OpenStack cluster ID generated for the Kubernetes cluster"
  value       = openstack_containerinfra_cluster_v1.cluster.id
}

# Print each Nodegroup id value
output "nodegroup_id" {
  description = "OpenStack nodegroup ID generated for the Kubernetes cluster"
  value       = [for nodegroup in openstack_containerinfra_nodegroup_v1.nodegroup : nodegroup.id]
}

output "kubeconfig" {
  description = "Kubernetes configuration"
  value       = openstack_containerinfra_cluster_v1.cluster.kubeconfig
  sensitive   = true
}
