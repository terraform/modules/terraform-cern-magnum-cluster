# Retrieve the Kubernetes template we are going to use

data "openstack_containerinfra_clustertemplate_v1" "cluster_template" {
  name = var.cluster_template_name
}
