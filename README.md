# terraform-cern-magnum-cluster module

Terraform module that deploys Kubernetes Magnum cluster on Openstack.

## Prerequisites

1. Create an Openstack project, and download the `Openstack clouds.yaml file` attached to this Openstack project.
2. A `keypair` will be needed to be able to create VMs in this Openstack project. To create one, you can follow the [Import a key pair](https://docs.openstack.org/horizon/latest/user/configure-access-and-security-for-instances.html#import-a-key-pair) procedure.
3. In the project you want to use this module, create a terraform file (e.g., `main.tf`), and add the following:
   
   ```hcl
   module "cluster" {
     source = "git::https://gitlab.cern.ch/terraform/modules/terraform-cern-magnum-cluster"

        cluster_name          = var.cluster_name
        cluster_template_name = var.cluster_template_name
        nodegroups_list       = var.nodegroups_list

        keypair = var.keypair

        # Master and minion node number corresponding to a machine
        master_count = var.master_count
        node_count   = var.node_count

        # Use cluster template values as default when the variables are not defined
        flavor        = var.flavor
        master_flavor = var.master_flavor

        # For creating certificates and keytab, 1 hour may not be enough
        create_timeout = var.create_timeout

        # Merge custom labels with the cluster template default labels
        labels = var.labels
   }
   ```

The above assumes a `variables.tf` file in the target repository using this module, including all definitions of the variables used.

## Input Parameters

### Openrc parameters

As mentioned in the prerequisites and as per the [`provider.tf`](./provider.tf), it assumes you will use a [`clouds.yaml`](https://docs.openstack.org/python-openstackclient/pike/configuration/index.html#configuration-files) file with a `openstack` cloud. Openstack clients look for this file in the following locations:

-  `~/.config/openstack/`
-  `/etc/openstack/`

The first file found wins.

It is also important to provide the `OS_USERNAME` environment variable in the machine running the Terraform plans.

### Cluster parameters

In order to provision a cluster, the following parameters must be provided in the `.tfvars` variables file.

| Name | Description |
|------|-------------|
| keypair | Keypair used for cluster creation |
| cluster_name | The name for the cluster |
| cluster_template_name | The name of the template that will be used for creating the cluster |
| labels | Custom labels to merge with template defaults. |
| create_timeout | Time after which the creation process is automatically stopped |
| master_flavor | The nova flavor id for booting the master or manager servers |
| flavor | The nova flavor id for booting the `default-worker` node servers |
| master_count | The number of servers that will run as master(s) in the cluster |
| node_count | The number of servers under the `default-worker` nodegroup, that will run as node in the cluster to host the users’ pods |
| nodegroups_list | Openstack Nodegroups definition |

`nodegroups_list` can be used in the `.tfvars` file as follows:

```hcl
nodegroups_list = [
  {
    merge_labels = "true",
    flavor_id    = "m2.medium",
    name         = "zone-a",
    node_count   = "1",
    labels = {
      availability_zone = "cern-geneva-a"
    }
  },
  {
    merge_labels = "true",
    name         = "zone-b",
    node_count   = "1",
    labels = {
      availability_zone = "cern-geneva-b"
    }
  },
  {
    merge_labels = "true"
    name         = "zone-c",
    node_count   = "1",
    labels = {
      availability_zone = "cern-geneva-c"
    }
  }
]
```

If no `nodegroup_list` is provided, just leave it empty with `nodegroups_list = []`.

For further information, check the [official documentation](https://docs.openstack.org/magnum/latest/user/index.html#cluster).

## Outputs

Output values are a way to expose some of that information to the user of the module.

Values exposed are:

| Name | Description |
|------|-------------|
| cluster_id | OpenStack cluster ID generated for the Kubernetes cluster |
| nodegroup_id | OpenStack nodegroup ID generated for the Kubernetes cluster |
| kubeconfig | The generated master kubeconfig for accessing the cluster |
