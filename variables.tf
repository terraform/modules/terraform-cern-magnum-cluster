# variables.tf defines the variables that must have values in order for your Terraform code to validate and run

#  Authenticatoin parameters

variable "keypair" {
  description = "Keypair used for cluster creation."
}

#  REQUIRED: Variables that must be specified when creating the cluster
variable "cluster_name" {
  description = "Name used for identifying the cluster."
}

variable "cluster_template_name" {
  description = "Cluster template name."
}

#  OPTIONAL: Custom labels to override or add to cluster creation

variable "labels" {
  description = "Custom labels to merge with template defaults."
}

#  OPTIONAL: Additional parameters used for fine-tuning the cluster
variable "create_timeout" {
  description = "Override for creation timeout."
  default     = 60
}

variable "master_flavor" {
  description = "Flavor to be used for the master nodes."
}

variable "flavor" {
  description = "Flavor to be used for the minions/nodes."
}

variable "region" {
  description = "The region in which to obtain the V1 Container Infra client. If omitted, the region argument of the provider is used."
  default     = null
}

variable "master_count" {
  description = "Number of master nodes to create."
  default     = 1
}

variable "node_count" {
  description = "Number of minion nodes to create."
  default     = 1
}

# Nodegroup list variable
# https://developer.hashicorp.com/terraform/language/expressions/type-constraints#example-nested-structures-with-optional-attributes-and-defaults
variable "nodegroups_list" {
  description = "OpenStack Nodegroups definition."
  type = list(object({
    docker_volume_size = optional(number),
    flavor_id          = optional(string),
    image_id           = optional(string),
    labels             = optional(object({ 
      availability_zone = optional(string)
      kubelet_options = optional(string)
    }),{}),
    max_node_count     = optional(number),
    merge_labels       = optional(bool)
    min_node_count     = optional(number),
    name               = string,
    node_count         = number,
    region             = optional(string),
    role               = optional(string)
  }))
}
