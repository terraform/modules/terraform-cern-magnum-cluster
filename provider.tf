# Configure the OpenStack Provider

# ffi: https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs
# using cloud = openstack means that clouds.yaml file must be present.
provider "openstack" {
  cloud = "openstack"
}